//
//  SpriteAppDelegate.h
//  SpriteWalkthrough
//
//  Created by Dmitry Simkin on 6/13/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpriteAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
