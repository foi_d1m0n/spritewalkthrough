//
//  main.m
//  SpriteWalkthrough
//
//  Created by Dmitry Simkin on 6/13/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SpriteAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SpriteAppDelegate class]));
    }
}
